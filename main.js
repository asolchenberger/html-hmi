// Modules to control application life and create native browser window
const {app, BrowserWindow, Menu, MenuItem, ipcMain, dialog} = require('electron');
//const EventEmitter = require('events');
const {AppManager} = require('./backend/app');

const appManager = new AppManager();

let mainMenuTemplate = [
  {label: "File",
    submenu: [
      {
        label: "devTools",
        accelerator: 'F12',
        click(item,focusedWindow)
          {focusedWindow.toggleDevTools();
          }
      }
    ]
  }
];
const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
Menu.setApplicationMenu(mainMenu);




