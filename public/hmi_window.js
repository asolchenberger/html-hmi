const electron = require('electron');
const EventEmitter = require( 'events' );
const {ipcRenderer} = electron;
const emitter = new EventEmitter();
emitter.setMaxListeners(1000);

ipcRenderer.on('TagSubscriptionUpdate', (e, payload)=>{
  emitter.emit('TagSubscriptionUpdate', JSON.parse(payload))
})

const writeToConnection = (connection, tag, value)=>{
  console.log("Sending tag value:")
  sendToBackend('writeToConnection', {connection:connection, tag:tag, value:value})
}

const requestTagIds = async (request)=>{
  //tag tagRequest = {window: "MAIN", tags:[{connection: "Some Connection", tag: "Some Tag"}]}
  ipcRenderer.on('returnTagIds', (e, returnPayload)=>{
    const reply = JSON.parse(returnPayload);
    if(request.window == reply.window){//this is my reply
      emitter.emit('returnTagIds', reply)
      console.log(reply)
    }
  })
  sendToBackend('requestTagIds-window', request);
}
const sendToBackend = (signal, payload)=>{
  console.log(payload)
  ipcRenderer.send(signal, JSON.stringify(payload));
}; 

const openKeypad = (connection, tag)=>{
  keypadKeyPressed = (key)=>{
    let screen = document.getElementById("keypad-screen")
    switch(key){
      case ".":
        if(!screen.innerHTML.includes(".")){
          screen.innerHTML += ".";
        }
        break;
      case "-/+":
        if(!screen.innerHTML.includes("-")){
          screen.innerHTML = "-" + screen.innerHTML;
        }
        else{
          screen.innerHTML = screen.innerHTML.slice(1)
        }
        break;

      default:
        screen.innerHTML += key;
    }
  }
  sendValue = ()=>{
    let screenText = document.getElementById("keypad-screen").innerHTML
    if(screenText.length && Number(screenText)){
      writeToConnection(connection, tag, Number(screenText));
      document.body.removeChild(bigDiv);
    }
  }
  const bigDiv = document.createElement("div")
  bigDiv.id = "keypad-div"
  let body = document.body,
    html = document.documentElement;
  let h = Math.max( body.scrollHeight, body.offsetHeight, 
    html.clientHeight, html.scrollHeight, html.offsetHeight );

  let w = Math.max( body.scrollWidth, body.offsetWidth, 
    html.clientWidth, html.scrollWidth, html.offsetWidth );
  bigDiv.style = `
  width: ${w}px; height: ${h}px;
  position: fixed;
  top:0px;
  left:0px;
  background-color:#0008;
  z-index: 101;`
  document.body.appendChild(bigDiv)
  const bgDiv = document.createElement("div")
  bgDiv.style = `
  width: 640px; height: 480px;
  position: fixed;
  top:${h/4}px;
  left:${w/4}px;
  z-index: 102;`
  bigDiv.appendChild(bgDiv)
  let keys = [["7","8","9"],["4","5","6"],["1","2","3"],["-/+","0","."]]
  keyScript = ""

  for(let r in keys){
    for(let k in keys[r]){
      keyScript += `
      <rect onclick="keypadKeyPressed('${keys[r][k]}');" fill="#AAA" x="${110+k*160}" y="${150+r*70}" width="100" height="50" rx="10" ry="10" stroke="#FFF" stroke-width="2"/>
      <text pointer-events="none" fill="#000" text-anchor="middle" font-size="36" font-family=${font} x="${160+k*160}" y="${190+r*70}">${keys[r][k]}</text>
      `
    }
  }
  bgDiv.innerHTML = `
  <svg width="640" height="640">
    <rect fill="#CCC" x="2" y="2" width="636" height="636" rx="20" ry="20" stroke="#000" stroke-width="2"/>
    <rect fill="#000" x="78" y="52" width="480" height="80" rx="10" ry="10" stroke="#CCC" stroke-width="2"/>
    <text id="keypad-screen" pointer-events="none" fill="#0F0" text-anchor="middle" font-size="48" font-family=${font} x="318" y="110"></text>
    ${keyScript}
    <rect onclick="document.getElementById('keypad-screen').innerHTML = '';" fill="#AAA" x="100" y="430" width="200" height="60" rx="10" ry="10" stroke="#FFF" stroke-width="2"/>
    
    <text  pointer-events="none" fill="#000" text-anchor="middle" font-size="24" font-family=${font} x="200" y="460">CLEAR</text>
    <rect onclick="document.getElementById('keypad-screen').innerHTML = document.getElementById('keypad-screen').innerHTML.slice(0,-1);"
      fill="#AAA" x="340" y="430" width="200" height="60" rx="10" ry="10" stroke="#FFF" stroke-width="2"/>
      <text pointer-events="none" fill="#000" text-anchor="middle" font-size="24" font-family=${font} x="440" y="460">BACK</text>
    <rect id="keypad-confirm" fill="#AAA" x="100" y="500" width="200" height="60" rx="10" ry="10" stroke="#FFF" stroke-width="2"/>
    <text  pointer-events="none" fill="#000" text-anchor="middle" font-size="24" font-family=${font} x="200" y="535">CONFIRM</text>
    <rect id="keypad-cancel" fill="#AAA" x="340" y="500" width="200" height="60" rx="10" ry="10" stroke="#FFF" stroke-width="2"/>
    <text pointer-events="none" fill="#000" text-anchor="middle" font-size="24" font-family=${font} x="440" y="535">CANCEL</text>
  </svg>`
  document.getElementById("keypad-confirm").addEventListener("click", sendValue, false)
  document.getElementById("keypad-cancel").addEventListener("click", ()=>{document.body.removeChild(bigDiv)}, false)
  //setTimeout(()=>{document.body.removeChild(bigDiv)}, 3000)
}