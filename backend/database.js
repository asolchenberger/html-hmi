const sqlite3 = require('sqlite3').verbose();
const EventEmitter = require('events');
const fs = require('fs');
const path = require('path')




class SqlDb extends EventEmitter{
  constructor(filepath){
    super();
    if(filepath == null){
      filepath = ":memory:"
    }
    this.sqlDb = new sqlite3.Database(filepath,  (err) => {
      if (err) {
        throw(err);
      }
      return this
      })

    //the following funtion runs a sql command and resolves to True if suceeds or rejects with error
    this.runSql = function(sql, params=[]){
      const db = this.sqlDb
      return new Promise((resolve, reject)=>
        db.run(sql, params, function(err, success){
          if(err){
            console.log(sql, params)
            reject(err);
          }
          else{
            resolve(this);//this will be {lastID: xx, changes: xx}  
          }
        }      
      ))
    }
    
    //the following funtion executes a sql query and resolves with results or rejects with err
    this.execSql = function(sql, params=[]){
      const db = this.sqlDb;
      return new Promise((resolve, reject)=>{
          db.all(sql, params, function(err, rows) {
          if(err){
            reject(err);
          }
          else{
            resolve(rows)
            }
          })
        })
    }
  }
}


module.exports = {SqlDb};
