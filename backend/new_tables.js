const tables = {
  ConnectionTypes:`
  CREATE TABLE [Connection Types] (
    [Connection Type] TEXT NOT NULL,
    PRIMARY KEY ([Connection Type])
    );`,
  Connections: `
    CREATE TABLE "Connections" (
      "ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
      "Connection"	TEXT NOT NULL,
      "Connection Type"	TEXT NOT NULL,
      "Description"	TEXT DEFAULT '',
      FOREIGN KEY("Connection Type") REFERENCES "Connection Types"("Connection Type")
    );`,
  ADSConnectionsParams:`
    CREATE TABLE "ADSConnectionsParams" (
      "ConnectionID"	INTEGER NOT NULL,
      "AMS"	TEXT NOT NULL,
      "Port"	NUMERIC NOT NULL DEFAULT 851,
      "Pollrate"	NUMERIC NOT NULL,
      PRIMARY KEY("ConnectionID"),
      FOREIGN KEY("ConnectionID") REFERENCES "Connections"("ID")
    );`,
  EthernetIPConnectionsParams:`
    CREATE TABLE "EthernetIPConnectionsParams" (
      "ConnectionID"	INTEGER NOT NULL,
      "Host"	TEXT NOT NULL,
      "Port"	NUMERIC NOT NULL DEFAULT 44818,
      "Pollrate"	NUMERIC NOT NULL,
      PRIMARY KEY("ConnectionID"),
      FOREIGN KEY("ConnectionID") REFERENCES "Connections"("ID")
    );`,
  ModbusRTUConnectionsParams: `
    CREATE TABLE "ModbusRTUConnectionsParams" (
      "Connection"	INTEGER NOT NULL,
      "Port"	NUMERIC NOT NULL DEFAULT 502,
      "StationID"	NUMERIC NOT NULL,
      "Pollrate"	NUMERIC NOT NULL,
      PRIMARY KEY("Connection"),
      FOREIGN KEY("Connection") REFERENCES "Connections"("ID")
    );`,
  ModbusTCPConnectionsParams:`
    CREATE TABLE "ModbusTCPConnectionsParams" (
      "ConnectionID"	INTEGER NOT NULL,
      "Host"	TEXT NOT NULL,
      "Port"	INTEGER NOT NULL DEFAULT 502,
      "StationID"	NUMERIC NOT NULL,
      "Pollrate"	NUMERIC NOT NULL,
      PRIMARY KEY("ConnectionID"),
      FOREIGN KEY("ConnectionID") REFERENCES "Connections"("ID")
    );`,
  DataTypes:`
    CREATE TABLE [Data Types] (
      [Data Type] TEXT NOT NULL,
      [Bytes] NUMERIC NOT NULL,
      [Modbus Registers] NUMERIC NOT NULL,
      [C Data Type] TEXT NOT NULL,
      PRIMARY KEY ([Data Type])
      );`,
  Tags:`
    CREATE TABLE "Tags" (
      "ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
      "Use"	BOOLEAN,
      "Tag"	TEXT NOT NULL,
      "Tag Alt Lang"	TEXT,
      "Description"	TEXT,
      "Description Alt Lang"	TEXT,
      "ConnectionID"	INTEGER NOT NULL,
      "Read Only"	BOOLEAN,
      "Data Type"	TEXT,
      "Units"	INTEGER,
      "Minimum"	TEXT,
      "Maximum"	TEXT,
      "New Value"	TEXT,
      "Saved Value"	INTEGER,
      FOREIGN KEY("Data Type") REFERENCES "Data Types"("Data Type"),
      FOREIGN KEY("ConnectionID") REFERENCES "Connections"("ID")
    );`,
  Groups:`
    CREATE TABLE "Groups" (
      "ID"	INTEGER PRIMARY KEY AUTOINCREMENT,
      "ConnectionID"	INTEGER,
      "Use"	BOOLEAN,
      "Group"	TEXT NOT NULL,
      "Group Alt Lang"	TEXT DEFAULT '',
      "Description"	TEXT DEFAULT '',
      "Description Alt Lang"	TEXT DEFAULT '',
      FOREIGN KEY("ConnectionID") REFERENCES "Connections"("ID")
    );`,
  ModbusFuncTypes:`
    CREATE TABLE ModbusFuncTypes (
      [Type] TEXT NOT NULL,
      [Function] NUMERIC NOT NULL,
      PRIMARY KEY ([Type])
      );`,
  ModbusPollGroups:`
    CREATE TABLE "ModbusPollGroups" (
      "PollGroup"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      "ConnectionID"	INTEGER,
      "Start Address"	NUMERIC,
      "Length"	NUMERIC,
      "Type"	TEXT NOT NULL,
      FOREIGN KEY("Type") REFERENCES "ModbusFuncTypes"("Type"),
      FOREIGN KEY("ConnectionID") REFERENCES "Connections"("ID")
    );`,
  ModbusTagParams:`
    CREATE TABLE "ModbusTagParams" (
      "TagID"	TEXT NOT NULL,
      "PollGroup"	NUMERIC NOT NULL,
      "Address"	NUMERIC NOT NULL,
      "Bit"	bit DEFAULT 0,
      PRIMARY KEY("TagID"),
      FOREIGN KEY("TagID") REFERENCES "Tags"("ID"),
      FOREIGN KEY("PollGroup") REFERENCES "ModbusPollGroups"("PollGroup")
    );`,
  TagGroupRelations:`
    CREATE TABLE "TagGroupRelations" (
      "TagID"	TEXT,
      "GroupID"	TEXT,
      PRIMARY KEY("TagID","GroupID"),
      FOREIGN KEY("TagID") REFERENCES "Tags"("ID"),
      FOREIGN KEY("GroupID") REFERENCES "Groups"("ID")
    );`
}