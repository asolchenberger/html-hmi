// create a tcp modbus client
const sqlite3 = require('sqlite3').verbose();
const Modbus = require('jsmodbus');
const net = require('net');
const fs = require('fs');
const EventEmitter = require('events');
const ads = require('node-ads');
const {SqlDb} = require('./database')


class Connection extends EventEmitter{
  constructor(appManager, params){
    super();
    this.appManager = appManager;
    this.connected = false;
    this.id = params.ID;
    this.pollrate = params.Pollrate;
    this.pollLoop = null;
    this.dataTypes = {BOOL: {bytes: 1, registers: 1, c_type: "boolean"},
                      DINT: {bytes: 4, registers: 2, c_type: "int32_t"},
                      INT: {bytes: 2, registers: 1, c_type: "int16_t"},
                      LINT: {bytes: 8, registers: 4, c_type: "int64_t"},
                      LREAL: {bytes: 8, registers: 4, c_type: "double"},
                      REAL: {bytes: 4, registers: 2, c_type: "float"},
                      SINT: {bytes: 1, registers: 1, c_type: "int8_t"},
                      STRING: {bytes: 80, registers: 40, c_type: "char[80]"},
                      UDINT: {bytes: 4, registers: 2, c_type: "uint32_t"},
                      UINT: {bytes: 2, registers: 1, c_type: "uint16_t"},
                      ULINT: {bytes: 8, registers: 4, c_type: "uint64_t"},
                      USINT: {bytes: 1, registers: 1, c_type: "uint8_t"}
                    }
    
    this.db = new SqlDb() //used for internal class sorting, etc.
    this.tagSubscriptions = [] //array of tag ids. If connected the poll loop updates any tags here
    this.updateSubscription = async (id)=>{}//override this for each connection
    this.updateSubscription()
    this.appManager.on('TagSubscriptionChanged', async (id)=>{this.updateSubscription(id)})
    this.writeTag = async (tagWrite)=>{
      //override for each connection
      try{
        throw(Error(`Write tag method not implented for this connection type`))
      }
      catch(err){
        console.log(err)
      }
    }
    this.updateTags = async ()=>{
      //orverride this method for each connection
      console.log(`ID ${this.id} should be updating ${this.tagSubscriptions.length} subsciptions, override this method`)
    }
    this.on('connected', ()=>{
      this.appManager.connectionConnected(this.id)
      this.connected = true 
    if(params.Pollrate != null){
      this.pollLoop = setInterval(this.updateTags, params.Pollrate, this)
    }
    })
    this.on('disconnected', ()=>{
      this.appManager.connectionDisconnected(this.id);
      if(this.pollLoop){
        clearInterval(this.pollLoop)
      }
      this.pollLoop = null;
      this.connected = false})
  }
}

class SimulatedConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params);
    this.connect = async ()=>{
      this.emit('connected', '');
    }
    
    this.disconnect = async ()=>{
      this.emit('disconnected', '');
    }
    this.updateTags = async ()=>{
      //this is where real connection should poll
      const ts = Date.now()
      let tagUpdates = {}
      for(let t in this.tagSubscriptions){
        tagUpdates[this.tagSubscriptions[t]] = {value: Math.random() * 10.0, timestamp: ts}
      }
      if(Object.keys(tagUpdates).length){
        this.appManager.updateTagValues(tagUpdates)
      }
    }
  }
}

class AdsConnection extends Connection{
  constructor(params){
    super(params);
    this.host = params.Host,
    this.amsNetIdTarget = params.TargetAms,
    this.amsNetIdSource = params.SourceAms,
    this.amsPortTarget= params.Port,
    this.timeout= params.timeout || 500;
    this.client = undefined;
    this.clientReady = ()=>{
      if(!this.client){throw(Error("Ads client not connected"))}
    }

    this.connect = ()=>{
      return new Promise((resolve, reject)=>{
      this.client = ads.connect(
        {
          host: this.host,
          amsNetIdTarget: this.amsNetIdTarget,
          amsNetIdSource: this.amsNetIdSource,
          amsPortTarget: this.amsPortTarget,
          timeout: this.timeout
        }, ()=>{
            this.emit('connected')
            resolve(this.client)
          })
      this.client.on('error',  function(error) {
        reject(error)})
      })
    }

    this.disconnect = ()=>{
      return new Promise((resolve, reject)=>{
      this.client.disconnect({}, ()=>{
            this.emit('disconnected')
            this.client = null
            resolve(this.client)
          })
      })
    }

    this.read = (handle)=>{
      return new Promise((resolve, reject)=>{
        this.clientReady()
        this.client.read(handle, (err, handle)=>{
          if(err){
            reject(err)
          }
          resolve(handle)})
      })
    }

    this.getHandlesByName = (names) =>{
      return new Promise((resolve, reject)=>{
        this.clientReady()
        let hndls = [];
        for (let n in names){
          hndls.push({symname: names[n]})
        }
        this.client.getHandles(hndls,
          function (error, handles) {
              if (error) {
                reject(error)
              } else if (handles.err) {
                  reject(handles.err)
              } else {
                  resolve(handles)
              }
          })
      })
    }   

    this.getSymbols = (names) =>{
      return new Promise((resolve, reject)=>{
        this.clientReady()
        this.client.getSymbols(
          function (error, symbols) {
              if (error) {
                reject(err)
              }
              else {
                  resolve(symbols)
              }
          })
      })
    }
    this.close = ()=>{
      if(this.client){
        this.client.end(()=>{this.emit('disconnected')})
      }
    }
  }
}

class EthernetIPConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params);
    this.updateTags = async ()=>{
      let ts = Date.now()
      this.tagSubscriptions.map((id)=>{
        this.appManager.db.runSql(`
        UPDATE Tags SET Value = ?, Timestamp = ?
        WHERE ID = ?;`, [Math.floor(Math.random()*10000)*0.01, ts, id])
      })
    }
    this.connect = async ()=>{
      this.emit('connected')
      this.connected = true
      setInterval(this.updateTags, 1000)
    }
    this.disconnect = async ()=>{
      this.emit('disconnected')
      this.connected = false
    }
  }
}

class ModbusTCPConnection extends Connection{
  constructor(appManager, params){
    super(appManager, params)
    var check_list = ["Host", "Port", "StationID"];
    for (var param in check_list){
      if (params[check_list[param]] == undefined){
        throw("Incorrect params for ModbusTCP connection");
      }
    }
    
    this.socket = new net.Socket();
    this.connection = new Modbus.client.TCP(this.socket, params.StationID, 1000);
    this.Host = params.Host
    this.Port = params.Port
    this.StationID = params.StationID
    this.pollgroups = {}
    this.optimizingActive = false
    this.connect = async ()=>{
      try{
        this.socket.connect({
          'host' : this.Host,
          'port' : this.Port,
          'autoReconnect': true})
      }
      catch(err){
        this.appManager.log(err.message, err.stack)
      }
    }
    this.updateSubscription = async (id)=>{
      if(this.id==id){
        this.tagSubscriptions = await this.appManager.tagSubscriptions.getSubscriptionTags(this.id)
        this.optimizePollgroups();
      }
    }

    this.optimizePollgroups = async ()=>{
      const ts = Date.now();
      try{
        if(this.optimizingActive){
          setTimeout(this.optimizePollgroups, 2000); //comeback later
        }
        this.optimizingActive = true;
        const newPollgroups = []
        await this.db.runSql(`DELETE FROM [TagParams]`, [])//clear the table
        for (var idx in this.tagSubscriptions){
          let tagRow = await this.appManager.db.execSql(`SELECT * FROM [ModbusTags] WHERE ID = ?`, [this.tagSubscriptions[idx]])
          if(tagRow.length){
            await this.db.runSql(`INSERT INTO [TagParams] ([ID], [DataType], [Function], [Address], [EndAddress], [Bit], [WordSwapped], [ByteSwapped]) 
            VALUES (?,?,?,?,?,?,?,?);`,
            [tagRow[0].ID, tagRow[0]["DataType"], tagRow[0].Function, Number(tagRow[0].Address), Number(tagRow[0].Address) + this.dataTypes[tagRow[0]["DataType"]].registers-1,
            tagRow[0].Bit, tagRow[0].WordSwapped, tagRow[0].ByteSwapped])
          }
        } 
        //console.table(await this.db.execSql(`SELECT * FROM [TagParams];`,[]))
        await this.db.runSql(`DROP TABLE IF EXISTS PollGroups;`)
        await this.db.runSql(`
        CREATE TABLE IF NOT EXISTS "PollGroups" (
          "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          "Function"	NUMERIC NOT NULL,
          "Address"	NUMERIC,
          "Length"	NUMERIC
          )`
          );
        const MAX_READ = 100;
        let res
        for (let func = 1; func < 5; func++){
          let start = 0
          let end = start+MAX_READ
          let keepGoing = true;
          while(keepGoing){
            res = await this.db.execSql(`SELECT MIN(Address) AS [Start] FROM TagParams WHERE [Function] = ? AND [Address] >= ?`,[func, start])
            keepGoing = res[0].Start !== null
            if(keepGoing){
              start = res[0].Start
              end = start+MAX_READ
              res = await this.db.execSql(`SELECT ID, Function, Address, EndAddress FROM TagParams WHERE [Function] = ? AND [Address] >= ? AND [EndAddress] < ? ORDER BY Address`,[func, start, end])
              if(res.length){
                let pg = await this.db.runSql(`INSERT INTO PollGroups (Function, Address, Length) VALUES (?,?,?)`, [res[0].Function, res[0].Address, res[res.length-1].EndAddress-res[0].Address+1])
                newPollgroups[pg.lastID] = {tags:[], function: res[0].Function, addr: res[0].Address, len: res[res.length-1].EndAddress-res[0].Address+1, data: null, timestamp: null}
                for (var row in res){
                  let tagRes = await this.db.execSql(`SELECT Address, DataType, Bit, WordSwapped, ByteSwapped FROM TagParams WHERE ID = ?`, [res[row].ID])
                  newPollgroups[pg.lastID].tags.push({
                    id:res[row].ID,
                    offset:(Number(tagRes[0].Address) -  Number(res[0].Address)) * 2 + Math.floor(tagRes[0].Bit/8), //byte offset = (tag address - pollgroup address)*2bytesPerReg +  
                    dataType: tagRes[0].DataType,
                    bit: Number(tagRes[0].Bit % 8),
                    wordSwapped: Number(tagRes[0].WordSwapped),
                    byteSwapped:  Number(tagRes[0].ByteSwapped)})
                  await this.db.runSql(`UPDATE TagParams
                  SET Pollgroup = ? WHERE ID = ?; `, [pg.lastID, res[row].ID]) 
                }     
              }
              start = end;
            }
          }
        }
        this.pollgroups = newPollgroups
      }
      catch(err){
        console.log(err)
      }
    
      this.optimizingActive = false;
    }

    this.readPollgroups = async ()=>{
      try{
        
        for(let g in this.pollgroups){
          let resp = null;
          let group = this.pollgroups[g]
          switch(group.Function){
            case "0":
              resp = await this.connection.readCoils(Number(group.addr), Number(group.len));
            case "1":
              resp = await this.connection.readDiscreteInputs(Number(group.addr), Number(group.len));
            case "2":
              resp = await  this.connection.readInputRegisters(Number(group.addr), Number(group.len));
            default:
              resp = await this.connection.readHoldingRegisters(Number(group.addr), Number(group.len));
          }          
          group.timestamp = Date.now()
          const buff = resp.response.body.valuesAsBuffer
          group.data = buff.swap16()//modbus traffic is 16-bit Big-Endian.
        }
      }
      catch(err){
        console.log(err)
      }
    }

    this.updateTags = async ()=>{
      try{
        if(this.optimizingActive){
          return null
        }
        //this is where real connection should poll
        let tagUpdates = {}
        await this.readPollgroups();
        for(let pg in this.pollgroups){
          for(let t in this.pollgroups[pg].tags){
            let tag = this.pollgroups[pg].tags[t]
            tagUpdates[tag.id] = {value: this.getTagValue(pg, t), timestamp: this.pollgroups[pg].timestamp}
          }
        }
        if(Object.keys(tagUpdates).length){
          //tagUpdates = {"46": {value: 9.99, timestamp: 150320402}, "85": {value: 10.99, timestamp: 150320402}}
          this.appManager.updateTagValues(tagUpdates)
        }
      }
      catch(err){
        console.log(err)
      }
    }

    this.getTagValue = (pg, t)=>{
      let group = this.pollgroups[pg] 
      let tag = group.tags[t]
      //console.table(tag)
      let val = null
      let buff = null
      switch(tag.dataType){
        case "BOOL":
          val = (0 != ((group.data.readUInt8(tag.offset) & 1)));
          break;
        case "USINT":
          buff = Buffer.alloc(1).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+1);
          val =  buff.readUInt8(0);
          break;
        case "SINT":
          buff = Buffer.alloc(1).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+1);
          val =  buff.readInt8(0);
          break;
        case "UINT":
          buff = Buffer.alloc(2).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+2);
          if(tag.byteSwapped){buff = buff.swap16();}
          val =  buff.readUInt16LE(0);
          break;
        case "INT":
          buff = Buffer.alloc(2).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+2);
          if(tag.byteSwapped){buff = buff.swap16();}
          val =  buff.readInt16LE(0);
          break;
        case "UDINT":
          buff = Buffer.alloc(4).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+4);
          if(tag.byteSwapped){buff = buff.swap16();}
          if(tag.wordSwapped){buff = buff.swap32();}
          val =  buff.readUInt32LE(0);
          break;
        case "DINT":
          buff = Buffer.alloc(4).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+4);
          if(tag.byteSwapped){buff = buff.swap16();}
          if(tag.wordSwapped){buff = buff.swap32();}
          val =  buff.readInt32LE(0);
          break;
        case "ULINT":
          buff = Buffer.alloc(8).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+8);
          if(tag.byteSwapped){buff = buff.swap16();}
          if(tag.wordSwapped){buff = buff.swap32();}
          val =  buff.readBigUInt64LE();
          break;
        case "LINT":
          buff = Buffer.alloc(8).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+8);
          if(tag.byteSwapped){buff = buff.swap16();}
          if(tag.wordSwapped){buff = buff.swap32();}
          val =  buff.readBigInt64LE(0);
          break;
        case "REAL":
          buff = Buffer.alloc(4).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+4);
          if(tag.byteSwapped){buff = buff.swap16();}
          if(tag.wordSwapped){buff = buff.swap32();}
          val =  buff.readFloatLE(0);
          break;
        case "LREAL":
          buff = Buffer.alloc(8).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+8);
          if(tag.byteSwapped){buff = buff.swap16();}
          if(tag.wordSwapped){buff = buff.swap32();}
          val =  buff.readDoubleLE(0);
          break;
        case "STRING":
          buff = Buffer.alloc(80).fill(0);
          group.data.copy(buff, 0, tag.offset, tag.offset+80);
          val = buff.toString('ascii')
          break;
        default:
          console.log(`Err: Unknown data type "${tag.dataType}"`)
        }
        return val
    }
    
    this.writeTag = async (tagWrite)=>{
      //{id: 1, value: "100.1"}
      try{
        let tagRes = await this.appManager.db.execSql(`
          SELECT * FROM ModbusTags WHERE ID = ? AND ConnectionID = ?;
        `, [tagWrite.id, this.id])
        if(tagRes.length){
          //this.encodeTagValue
          let tag = tagRes[0]
          let mask = 0x00000000;//used for single bit writes
          let val = 0x00000000;
          let resp; //use if the register value now is needed
          let buff = Buffer.alloc(8).fill(0);
          switch(true){
            case tag.DataType == "BOOL" && tag.Function == "3"://need to read the register to set a single bit
              resp = await this.connection.readHoldingRegisters(Number(tag.Address), 1);
              mask = resp.response.body.valuesAsArray[0]; //read the register then set or clear the proper bit
              if(!Number(tagWrite.value)){
                val = mask & (0xFFFFFFFF ^ 1<<tag.Bit)
              }
              else{
                val = mask | 1<<tag.Bit
              }
              await this.connection.writeSingleRegister(tag.Address, val)
              break;
            case tag.DataType == "SINT" && tag.Function == "3":
              //TODO
              break;
            case (tag.DataType == "USINT" && tag.Function == "3"):
              //TODO
              break;
            case (tag.DataType == "INT" && tag.Function == "3"):
              buff.writeInt16LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,2))
              break;
            case (tag.DataType == "UINT" && tag.Function == "3"):
              buff.writeUInt16LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,2))
              break;
            case (tag.DataType == "DINT" && tag.Function == "3"):
              buff.writeInt32LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,4))
              break;
            case (tag.DataType == "UDINT" & tag.Function == "3"):
              buff.writeUInt32LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,4))
            case (tag.DataType == "LINT" && tag.Function == "3"):
              buff.writeBigInt64LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,8))
              break;
            case (tag.DataType == "ULINT" && tag.Function == "3"):
              buff.writeBigUInt64LE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,8))
              break;
            case (tag.DataType == "REAL" && tag.Function == "3"):
              buff.writeFloatLE(tagWrite.value)
              if(!tag.ByteSwapped){buff = buff.swap16();}
              if(tag.WordSwapped){buff = buff.swap32();}
              await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,4))
              break;
            case (tag.DataType == "LREAL" && tag.Function == "3"):
            buff.writeDoubleLE(tagWrite.value)
            if(!tag.ByteSwapped){buff = buff.swap16();}
            if(tag.WordSwapped){buff = buff.swap32();}
            await this.connection.writeMultipleRegisters(tag.Address, buff.slice(0,8))
            break;
            case (tag.Function == "0")://coil write
              await this.connection.writeSingleCoil(tag.Address, tagWrite.value) 
              break;
            default:
              console.log("Write function not impemented yet")
              break;            
          }
        }
        
      }
      catch(err){
        console.log(err)
      }
    }

    this.intiDb = async ()=>{
      try{
        await this.db.runSql(`PRAGMA foreign_keys = True;`)
        await this.db.runSql(`
        CREATE TABLE "TagParams" (
          "ID"	NUMERIC NOT NULL,
          "Function" INTEGER,
          "PollGroup"	INTEGER,
          "DataType" TEXT,
          "Address"	INTERGER NOT NULL,
          "EndAddress" INTEGER,
          "Bit"	BIT DEFAULT 0,
          "WordSwapped" BIT,
          "ByteSwapped" BIT,
          PRIMARY KEY("ID")
        );`)
        await this.db.runSql(`
        CREATE TABLE IF NOT EXISTS "PollGroups" (
          "ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          "Function"	NUMERIC NOT NULL,
          "Address"	NUMERIC,
          "Length"	NUMERIC
          );`)
      }
      catch(err){
        this.appManager.log(err.message, err.stack)
      }
    }

    this.intiDb()
    /*
    functions = 0: coil (0xxxxx)
                1: input staus (1xxxxx)
                3: holding register (4xxxxx)
                4: input register (3xxxxx)
    */
    this.disconnect = async ()=>{
      this.socket.end();
    };
     
    this.socket.on('close', ()=>{
      this.emit('disconnected');
    })
    this.socket.on('connect', ()=> {
      this.emit('connected');
    })

    this.socket.on('error', (err)=>{
      var msg = `Modbus connection error in connection id ${this.id}: `;
      /* if (options.autoReconnect) {
        setTimeout(this.connect, 30000);
        msg += err.message + '\nRetrying in 30 seconds.'
      } */
      console.log(msg);  
    })
  }
}

class TagSubscriptions extends EventEmitter{
  constructor(appManager){
    super();
    this.appManager = appManager
    this.addSubscription = async (tagIds)=>{
      try
        {//each subsciption gets an ID back from inserting a Subscription.
        let res = await this.appManager.db.runSql(`INSERT INTO TagSubscriptions VALUES (NULL)`)
        let subId = res.lastID
        this.changeSubscription(subId,tagIds)
        return subId}
      catch(err){
        console.log(err.message, err.stack)
      }
    }

    this.removeSubscription = async (subId, listener)=>{
      try
        {await this.appManager.db.runSql(`DELETE FROM "TagSubscriptions" WHERE SubscriptionID = ?`, [subId]).catch(err=>console.log(err))
        this.removeListener(listener)
      }
      catch(err){
        console.log(err.message, err.stack)
      }
    }

    this.changeSubscription = async (subId, tagIds) =>{
      try{
        let subCheck = await this.appManager.db.execSql(`SELECT (ID) FROM TagSubscriptions WHERE ID = ?;`, [subId])
        if(!subCheck.length){
          return
        }
        await this.appManager.db.runSql(`DELETE FROM "TagSubRelations" WHERE SubID = ?`, [subId])//remove the old sub-tag rows
        .catch(err=>console.log(err))
        
        //console.table(tagRows)
        for(let t in tagIds){
          let tagCheck = await this.appManager.db.execSql(`SELECT (ID) FROM Tags WHERE ID = ?;`, [tagIds[t]])
          if(tagCheck.length){//if this tag is in the Tags table
            await this.appManager.db.runSql(`INSERT INTO TagSubRelations (SubID, TagID) VALUES (?,?)`, [subId, tagIds[t]])
          }
        }
        //emit the connection ids of all connections involved
        let connRes = await this.appManager.db.execSql(`SELECT DISTINCT Tags.ConnectionID FROM TagSubRelations 
        INNER JOIN Tags ON Tags.ID = TagSubRelations.TagID 
        WHERE TagSubRelations.SubID = ?;`, [subId])
        for(let row in connRes){
          this.appManager.emit('TagSubscriptionChanged', connRes[row].ConnectionID)
        }
      }
      catch(err){
        console.log(err.message + err.stack)
      }
    }

    this.getSubscriptionTags = async (connId)=>{
      //gather all tags this connection need to poll
      try{
        let res = await this.appManager.db.execSql(`
        SELECT DISTINCT TagSubRelations.TagID FROM TagSubRelations 
          INNER JOIN Tags ON Tags.ID = TagSubRelations.TagID 
          WHERE Tags.ConnectionID = ?;`, [connId])
          const tags = []
          for(let row in res){
            tags.push(res[row].TagID)
          }
          return tags}
      catch(err){
        console.log(err)
      }
    }
    

    this.updateConnections = async (connectionIds)=>{
      //call this when the connected status of a connection changes
      //if it exists in the appManager.connections it assumes it's connected
      try{
        connectionIds.map(async (id)=>{
          //get the tags in subs with this id
          let tagRows = await this.appManager.db.execSql(`SELECT ID FROM Tags INNER JOIN TagSubRelations ON Tags.ID = TagSubRelations.TagID WHERE Tags.ConnectionID = ?;`, [id])
          .catch(err=>console.log(err))
          if(!this.appManager.connections[id]){ //if connection not open
            let ts = Date.now()
            this.appManager.updateTagValues(tagRows.map((tagRow)=>{
              return {tagId: tagRow.ID, value: null, timestamp: ts}
            }))//set tag values to bad quality
          }
        })
        
      }
      catch(err){
        console.log(err.message, err.stack)
      }
    }

  }
}

module.exports = {
  SimulatedConnection,
  AdsConnection,
  ModbusTCPConnection,
  EthernetIPConnection,
  TagSubscriptions}



                    