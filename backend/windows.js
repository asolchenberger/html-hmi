const {app, BrowserWindow, Menu, MenuItem, dialog, ipcMain} = require('electron');
const EventEmitter = require('events');
const {SqlDb} = require('./database');

class DataSubcriber extends EventEmitter{
  //class to subcribe to tag updates
  //if multiple different updates are needed for a page, etc use array of these
  constructor(appManager){
    super()
    this.appManager = appManager
    this.subId = null
    this.subListener = null
    this.newSubscription = async (tagIds)=>{
      //tell the appManager what tags you want to listen for
      this.subId = await this.appManager.tagSubscriptions.addSubscription(tagIds).catch(err=>console.log('Error adding sub: ',err))
      this.subListener = this.appManager.on('TagSubscriptionUpdate',async (update)=>{
      if(update.subId === this.subId){
        await this.tagSubUpdate(update.tagIds).catch(err=>{console.log(err)})
        }
      })
    }
    this.changeSubscription = async (tagIds)=>{
      if(!this.subId){return}
      this.appManager.tagSubscriptions.changeSubscription(this.subId, tagIds)
    }
    this.removeSubscription = ()=>{
      this.appManager.tagSubscriptions.removeSubscription(this.subId, this.subListener)    
    }
    
    this.tagSubUpdate = async (tagIds)=>{
      //new tag values were updated. Override this method to do want you want
      const vals = {}
      for(let t=0;t<tagIds.length; t++){
        let res = await this.appManager.db.execSql(`SELECT ID, Value, Timestamp FROM Tags WHERE ID = ?;`, [tagIds[t]])
        if(res[0]){
          vals[tagIds[t]] = {value: res[0].Value, timestamp: res[0].Timestamp}
        }
      }
      this.sendToWindow('TagSubscriptionUpdate', vals)

    }
    this.tagIdRequest = async (request)=>{
      try{      
        const returnRes = {}
        for(let t in request.tags){
          let res = await this.appManager.db.execSql(`
          SELECT Connections.Connection, Tags.Tag, Tags.ID FROM
          Tags INNER JOIN Connections ON Tags.ConnectionID = Connections.ID
          WHERE Connections.Connection = ? AND Tags.Tag = ?;`, [request.tags[t].connection, request.tags[t].tag])
          if(res.length){
            if(!returnRes[res[0].Connection]){
              returnRes[res[0].Connection] = {};
            }
            returnRes[res[0].Connection][res[0].Tag] = res[0].ID
          }
        }
        if(returnRes !== {}){
          this.sendToWindow('returnTagIds', {window: request.window, connections:returnRes})
        }
      }
      catch(e){
        console.log(e)
      }
    }
  }
}

class Window extends DataSubcriber{
  constructor(params){
    super(params.appManager)
    this.appManager = params.appManager
    const defaults = {
      minWidth: 0,
      minHeight: 0,
      frame: false,
      bg_color: "#404040",
      resizable: true,
    }
    for (var param in defaults){
      if (params[param] == undefined){
        params[param] = defaults[param]}
      }
      this.window = new BrowserWindow({
      width: params.width,
      height: params.height,
      minHeight: params.minHeight,
      minWidth: params.minWidth,
      title: params.title,
      frame: params.frame,
      backgroundColor: params.bg_color,
      resizable: params.resizable,
      alwaysOnTop: params.alwaysOnTop,
      webPreferences: {   
        nodeIntegration: true
      }
    })
    this.appManager.on('appDbReady', (e)=>{
      this.newSubscription([]) //ask for a subscriptions once db ready
    })
    this.sendToWindow = (signal, payload)=>{
      if(!this.window.isDestroyed()){
        this.window.webContents.send(signal, JSON.stringify(payload));
      }
    }
    this.window.loadFile(params.html)
    this.window.on('closed', ()=>{this.appManager.closeWindow(params.id)})
  }

}

class MainWindow extends Window{
  constructor(params){
    params.id = "MAIN";
    params.html ='./public/index.html';
    params.title = "HMI";
    params.width= 1600;
    params.height= 900;
    params.minWidth= 1024;
    params.minHeight= 565;
    params.frame = true;
    super(params);
    this.closeApp= async()=>{
      setTimeout(app.quit, 100);
    }
    
  }
}



module.exports = {MainWindow}
